<?php

namespace MageMastery\FirstModule\Controller\Page;

use Magento\Framework\App\Action\Action;
use Magento\Framework\Controller\Result\Json;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\View\Result\Page;

class Index  extends Action
{

    public function execute()
    {
        /** @var Json $jsonResult */
        /*$jsonResult = $this->resultFactory->create(ResultFactory::TYPE_JSON);

        $jsonResult->setData([
            'message'=>'My first Page'
        ]);
        return $jsonResult;*/
        /** @var Page $resultPage */
        return $this->resultFactory->create(ResultFactory::TYPE_PAGE);

    }
}
